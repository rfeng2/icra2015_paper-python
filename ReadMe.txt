-- This demo shows the performance of keypoint matching method in [1], using part of the dataset in [2]. (Tested with Ubuntu 14.04, Python 2.7.4, Opencv 2.4.9)

-- Example:
>> python decideMatch.py img1.ppm img6.ppm

   Expected output:
   img1 - 2461 features, img2 - 1152 features
   final matching number is 542


-- The ground truth matches are determined by using the homography matrices provided with the dataset. The first image (img1.ppm) is transformed to match the other 5 images (img2.ppm to img6.ppm). In order to build the ground truth, we first calculate the nearest neighbor matches and then apply the homography constraint to remove false matches. Lowe's original implementation of SIFT detector and descriptor are used [3].

-- Comments are given in the code to make it self-explanatory.


If you have any further questions regarding the code or paper, please send an email to liu17@cs.ualberta.ca


[1] Yang Liu, Rong Feng and Hong Zhang, "Keypoint Matching by Outlier Pruning with Consensus Constraint", IEEE International Conference on Robotics and Automation (ICRA), Seattle, USA, May 2015.

[2] http://www.robots.ox.ac.uk/~vgg/research/affine/

[3] http://www.cs.ubc.ca/~lowe/keypoints/
