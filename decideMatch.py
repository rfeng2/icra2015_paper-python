import numpy as np
import cv2
import sys
from math import exp,pow,log10
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
###############################################################################
# Image Matching
###############################################################################

def match_images(img1, img2):
    """Given two images, returns the matches"""
    detector = cv2.SIFT()
    matcher = cv2.BFMatcher()

    kp1, desc1 = detector.detectAndCompute(img1, None)
    kp2, desc2 = detector.detectAndCompute(img2, None)
    print 'img1 - %d features, img2 - %d features' % (len(kp1), len(kp2))

    raw_matches = matcher.knnMatch(desc1, trainDescriptors = desc2, k = 2) #2
    kp_pairs = filter_matches(kp1, kp2, raw_matches)
    #print len(raw_matches)
    return kp_pairs

def filter_matches(kp1, kp2, matches, ratio = 1.0):
    mkp1, mkp2 = [], []
    for m in matches:
        if len(m) == 2 and m[0].distance < m[1].distance * ratio:
            m = m[0]
            mkp1.append( kp1[m.queryIdx] )
            mkp2.append( kp2[m.trainIdx] )

    # find_mode_quick #

    delx_list, dely_list,z_list = [], [], []
    
    for i in range(0,len(mkp1)):
        delx_list.append(int(mkp2[i].pt[0]-mkp1[i].pt[0]))
	dely_list.append(int(mkp2[i].pt[1]-mkp1[i].pt[1]))

    delx_r = max(delx_list)-max(delx_list)%5+5
    delx_l = min(delx_list)-min(delx_list)%5
    dely_r = max(dely_list)-max(dely_list)%5+5
    dely_l = min(dely_list)-min(dely_list)%5

    x_grid = (delx_r-delx_l)/5+1 
    y_grid = (dely_r-dely_l)/5+1
    bin = [[0]*(y_grid) for row in range(x_grid)]

    max_v = 0
    max_bin_x = 0
    max_bin_y = 0
    xx_list, yy_list = [], []
    for i in range(0,len(delx_list)):
	xx = int(np.floor((delx_list[i]-delx_l)/5)+1)
        yy = int(np.floor((dely_list[i]-dely_l)/5)+1)
        xx_list.append(delx_list[i])
        yy_list.append(dely_list[i])
        bin[xx][yy] = bin[xx][yy]+1
	if( bin[xx][yy] > max_v):
            max_v = bin[xx][yy]
            max_bin_x = delx_list[i]
            max_bin_y = dely_list[i]
    
    #print 'highest bin'
    #print max_bin_x,max_bin_y

    # meanshift #

    for loop in range(1000):
	w_sum = 0.
        w_array = np.zeros(len(delx_list))
        for i in range(0,len(delx_list)):
	    xx = xx_list[i]
            yy = yy_list[i]
	    w = exp( -( pow((xx-max_bin_x),2)+pow((yy-max_bin_y),2) )/ pow(100,2) )
            w_array[i] = w
	    w_sum = w_sum + w
            
        mx, my = 0,0
        for i in range(0,len(delx_list)):
	    xx = xx_list[i]
            yy = yy_list[i]
            mx = mx+w_array[i]*xx
            my = my+w_array[i]*yy
        mx = mx/w_sum
        my = my/w_sum
     
        if( abs(mx-max_bin_x)+abs(my-max_bin_y) < 0.00001):
                break
        else:
		max_bin_x = mx
                max_bin_y = my

    #print 'after meanshift'
    #print max_bin_x,max_bin_y

    # Laplacian filtering #

    mu_x, mu_y, mu_num= 0,0,0
    sample_x, sample_y = [],[]
    for i in range(0,len(delx_list)):
	xx = xx_list[i]
        yy = yy_list[i]
        w = w_array[i]
        if( w> 0.5):
            mu_x = mu_x + xx
            mu_y = mu_y + yy
            mu_num = mu_num +1
            sample_x.append(xx)
            sample_y.append(yy)
    mu_x = mu_x / mu_num
    mu_y = mu_y / mu_num
    
    var_x,var_y = 0,0
    for i in range(len(sample_x)):
        var_x = var_x + abs(sample_x[i]-mu_x)
        var_y = var_y + abs(sample_y[i]-mu_y)
    var_x = var_x / mu_num
    var_y = var_y / mu_num
  
    
    # Get boundary #

    p = 0.99
    max_b_x = -log10(2*(1-p)) * var_x + mu_x
    min_b_x = 2*mu_x - max_b_x
    max_b_y = -log10(2*(1-p)) * var_y + mu_y
    min_b_y = 2*mu_y - max_b_y
    #print 'after Laplacian' 
    #print min_b_x,max_b_x
    #print min_b_y, max_b_y

    final_index = []
    for i in range(0,len(delx_list)):
        xx = xx_list[i]
        yy = yy_list[i]
        if( xx > min_b_x and xx < max_b_x and yy > min_b_y and yy < max_b_y):
            final_index.append(i)
          

    kp_pairs = zip(mkp1, mkp2)
    return kp_pairs,final_index
    
    
###############################################################################
# Match Diplaying
###############################################################################

def explore_match2(win, img1, img2, kp_pairs,final_index, status = None, H = None):
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]
    vis = np.zeros((max(h1, h2), w1+w2), np.uint8)
    vis[:h1, :w1] = img1
    vis[:h2, w1:w1+w2] = img2
    vis = cv2.cvtColor(vis, cv2.COLOR_GRAY2BGR)

    frp1, frp2 = zip(*kp_pairs)
    p1 = np.float32([kp.pt for kp in frp1])
    p2 = np.float32([kp.pt for kp in frp2])

    #f = open('data.txt','r+')

    for i in range(0,len(p1)):
        
        #color = tuple([sp.random.randint(0, 255) for _ in xrange(3)])
        if (i in final_index):
		#print 'p1:', p1[i][0],p1[i][1]
		#print 'p2:', p2[i][0],p2[i][1]
		#f.write(str(int(p1[i][0]))+','+str(int(p1[i][1]))+' '+str(int(p2[i][0]))+','+str(int(p2[i][1]))+'\n')
        	color = (255,0,0) 
		cv2.circle(vis,(p1[i][0],p1[i][1]), 2, color, -1)
    		cv2.circle(vis,(int(p2[i][0]+w1),p2[i][1]), 2, color, -1)
        	cv2.line(vis,(p1[i][0],p1[i][1]),(int(p2[i][0]+w1),p2[i][1]),color,1)
		
	'''else:
		color = (0,255,0)
		cv2.circle(vis,(p1[i][0],p1[i][1]), 2, color, -1)
    		cv2.circle(vis,(int(p2[i][0]+w1),p2[i][1]), 2, color, -1)
        	cv2.line(vis,(p1[i][0],p1[i][1]),(int(p2[i][0]+w1),p2[i][1]),color,1)'''
    #f.close()   

    print 'final matching number is %d' %len(final_index) 

 
    cv2.imshow(win, vis)


  
def draw_matches(window_name, kp_pairs,final_index, img1, img2):
    """Draws the matches for """
    mkp1, mkp2 = zip(*kp_pairs)
    
    p1_old = np.float32([kp.pt for kp in mkp1])
    p2_old = np.float32([kp.pt for kp in mkp2])
    
    p1 = np.float32([(0.0,0.0)]*len(final_index))
    p2 = np.float32([(0.0,0.0)]*len(final_index))
    
   
    index = 0
    index2 = 0
    for index in range(len(p1)):
        if (index in final_index):
                #print row
		p1[index2] = p1_old[index]
                p2[index2] = p2_old[index]
                index2 += 1
        index += 1
    
    
    if len(kp_pairs) >= 4:
        H, status = cv2.findHomography(p1, p2, cv2.RANSAC, 5.0)
        #print H
        #print '%d / %d  inliers/matched' % (np.sum(status), len(status))
    else:
        H, status = None, None
        #print '%d matches found, not enough for homography estimation' % len(p1)
    
    if len(p1):
        explore_match2(window_name, img1, img2, kp_pairs,final_index, status, H)

###############################################################################
# Test Main
###############################################################################

if __name__ == '__main__':
    """Test code: Uses the two specified"""
    if len(sys.argv) < 3:
        print "No filenames specified"
        print "USAGE: find_obj.py <image1> <image2>"
        sys.exit(1)
    
    fn1 = sys.argv[1]
    fn2 = sys.argv[2]

    img1 = cv2.imread(fn1, 0)
    img2 = cv2.imread(fn2, 0)
    
    if img1 is None:
        print 'Failed to load fn1:', fn1
        sys.exit(1)
        
    if img2 is None:
        print 'Failed to load fn2:', fn2
        sys.exit(1)

    kp_pairs,final_index = match_images(img1, img2)
    
    if kp_pairs:
        draw_matches('find_obj', kp_pairs, final_index, img1, img2)
        cv2.waitKey()
        cv2.destroyAllWindows()    
else:
        print "No matches found"
    
